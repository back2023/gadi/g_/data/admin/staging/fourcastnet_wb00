mkdir -p  ${HOME}/fourcastnet_notebook
mkdir -p  ${HOME}/fourcastnet_notebook/FourCastNet_Fig
cp -n /g/data/wb00/admin/staging/FourCastNet_wb00/FourCastNet-01.ipynb  ${HOME}/fourcastnet_notebook/
cp -n /g/data/wb00/admin/staging/FourCastNet_wb00/FourCastNet_Fig/FourCastNet-1.png  ${HOME}/fourcastnet_notebook/FourCastNet_Fig/FourCastNet-1.png
cp -n /g/data/wb00/admin/staging/FourCastNet_wb00/FourCastNet_Fig/FourCastNet-2.png  ${HOME}/fourcastnet_notebook/FourCastNet_Fig/FourCastNet-2.png
cp -n /g/data/wb00/admin/staging/FourCastNet_wb00/FourCastNet_Fig/FourCastNet-3.png  ${HOME}/fourcastnet_notebook/FourCastNet_Fig/FourCastNet-3.png
chmod 775 ${HOME}/fourcastnet_notebook/FourCastNet-01.ipynb
chmod 775 ${HOME}/fourcastnet_notebook/FourCastNet_Fig/FourCastNet-1.png
chmod 775 ${HOME}/fourcastnet_notebook/FourCastNet_Fig/FourCastNet-2.png
chmod 775 ${HOME}/fourcastnet_notebook/FourCastNet_Fig/FourCastNet-3.png
